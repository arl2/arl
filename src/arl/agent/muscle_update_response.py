from __future__ import annotations
from typing import TYPE_CHECKING, Any

from dataclasses import dataclass


@dataclass
class MuscleUpdateResponse:
    adaptive_policy_update: Any
    rules_policy_update: Any
