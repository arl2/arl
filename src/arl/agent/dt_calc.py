import logging

from nn2eqcdt import Tree
from nn2eqcdt.helper_methods.check_model import check_model
from nn2eqcdt.helper_methods.plot_methods import plot_with_pyvis
from nn2eqcdt.models.PathCheckingOptions import PathCheckingOptions

from palaestrai.agent.brain import Brain
import numpy as np

from torch import nn

LOG = logging.getLogger(__name__)


def get_constraints(brain: Brain):
    constraints = {}

    for sensor in brain.sensors:
        assert len(brain.sensors[0].space.shape) == 1, "Multi-dim shapes for SensorInformation are not supported yet"
        len_collapsed_sensor_info = np.array(brain.sensors[0].space.shape).item()
        len_constraints_old = len(constraints)
        for i in range(len_collapsed_sensor_info):
            constraints[len_constraints_old + i] = (sensor.space.low[i], sensor.space.high[i])
    LOG.debug(f"Constraints: {str(constraints)}")

    return constraints


def calc_dt(brain: Brain):

    assert brain is not None

    if hasattr(brain, 'get_seq'):
        model = brain.get_seq()
        assert type(model) is nn.Sequential
        constraints = get_constraints(brain)

        try:
            model.eval()
            check_model(model)

            path_checking_options = PathCheckingOptions(model, constraints=constraints)

            tree = Tree(path_checking_options, processes=1)
            tree.build_tree(model)

            tree.remove_unsat_paths()

            expr_node_volume_dict = tree.calc_properties()

            html_plot = plot_with_pyvis(tree.get_T())
        except Exception as e:
            LOG.error("The tree could not properly be transformed: ", e)
        LOG.info(f"TAR tree computed: {str(tree)}")
    else:
        LOG.warning(f"No get_seq method found for: {str(brain)}")
