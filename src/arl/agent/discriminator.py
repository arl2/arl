from __future__ import annotations
from typing import TYPE_CHECKING, Any, List, Tuple, Optional

import copy
import logging
import numpy as np

from midas_palaestrai.reward import Reward
from palaestrai.agent import RolloutWorker, Memory
import palaestrai.types

if TYPE_CHECKING:
    from palaestrai.agent import (
        Muscle,
        Objective,
        SensorInformation,
        ActuatorInformation,
    )
    from .world_model import WorldModel

from .decision import Decision, InfererChoice
from .decision_proposal import DecisionProposal

LOG = logging.getLogger(__name__)


class Discriminator:
    DEFAULT_PT1_PERIOD = 3

    def __init__(
            self,
            adaptive_inferer: Muscle,
            rules_inferer: Muscle,
            world_model: WorldModel,
            reward: Reward,
            objective: Objective,
            imitation_steps: Optional[int] = None,
    ):
        self.uid: str = ""
        self._rules_inferer = rules_inferer
        self._rules_inferer_trust = 0.5
        self._adaptive_inferer = adaptive_inferer
        self._adaptive_inferer_trust = 0.49
        self._world_model = world_model
        self._reward = reward
        self._objective = objective
        self._imitation_steps = imitation_steps
        self._pt1_period = Discriminator.DEFAULT_PT1_PERIOD

        self._memory: Memory = Memory(1)

    @property
    def adaptiver_inferer_trust(self) -> float:
        return self._adaptive_inferer_trust

    @property
    def rules_inferer_trust(self) -> float:
        return self._rules_inferer_trust

    @staticmethod
    def _pt1(y: float, u: float, t: float):
        """A simple LTI that averages over t."""
        return y + ((u - y) / t) if (t != 0.0) else u

    def propose_actions(
        self,
        sensor_readings: List[SensorInformation],
        actuators_available: List[ActuatorInformation],
    ) -> Decision:
        (
            adaptive_proposal_setpoints,
            adaptive_data,
        ) = RolloutWorker.try_propose_actions(
            self._adaptive_inferer,
            copy.deepcopy(sensor_readings),
            copy.deepcopy(actuators_available),
        )
        adaptiver_inferer_proposal = DecisionProposal(
            sensor_readings=sensor_readings,
            actuator_setpoints=adaptive_proposal_setpoints,
            expected_rewards=[],
            expected_objective=0.0,
            data_for_brain=adaptive_data,
        )

        (
            rules_proposal_setpoints,
            rules_data,
        ) = RolloutWorker.try_propose_actions(
            self._rules_inferer,
            copy.deepcopy(sensor_readings),
            copy.deepcopy(actuators_available),
        )
        rules_inferer_proposal = DecisionProposal(
            sensor_readings=sensor_readings,
            actuator_setpoints=rules_proposal_setpoints,
            expected_rewards=[],
            expected_objective=0.0,
            data_for_brain=rules_data
            if rules_data
            else (
                adaptive_data[0],
                np.array([a.value for a in rules_proposal_setpoints]),
            ),
        )

        # This wraps the trust-based decision based on predictive evaluation
        # with a preceding rules inferer phase to fill the memory for
        # the adaptive brain to imitate.
        # This avoids the PandaPowerWorldModel evaluation does currently not
        # support more complex midas elements.
        if self._imitation_steps is None:
            _decision: Decision = self._decide(
                adaptiver_inferer_proposal, rules_inferer_proposal
            )
        else:
            _inferer = InfererChoice.RULES
            applied_setpoints = rules_inferer_proposal.actuator_setpoints
            if (self._adaptive_inferer._actions_proposed >
                    self._imitation_steps):
                _inferer = InfererChoice.ADAPTIVE
                applied_setpoints = (adaptiver_inferer_proposal.
                                     actuator_setpoints)
            _decision = Decision(
                inferer=_inferer,
                setpoints=applied_setpoints,
                rules_inferer_proposal=rules_inferer_proposal,
                adaptive_inferer_proposal=adaptiver_inferer_proposal,
            )
        return _decision

    def _decide(
        self,
        adaptive_proposal: DecisionProposal,
        rules_proposal: DecisionProposal,
    ):
        state_after_adaptive = self._world_model.evaluate(
            adaptive_proposal.sensor_readings,
            adaptive_proposal.actuator_setpoints,
        )
        state_after_rules = self._world_model.evaluate(
            rules_proposal.sensor_readings, rules_proposal.actuator_setpoints
        )

        # Calculate reward & objective for that proposal:

        LOG.debug("%s calculating objective for adaptive policy", self)
        adaptive_objective, adaptive_rewards = self._calc_objective(
            adaptive_proposal.actuator_setpoints, state_after_adaptive
        )
        adaptive_proposal.expected_rewards = adaptive_rewards
        adaptive_proposal.expected_objective = adaptive_objective

        LOG.debug("%s calculating objective for rules policy", self)
        rules_objective, rules_rewards = self._calc_objective(
            rules_proposal.actuator_setpoints, state_after_rules
        )
        rules_proposal.expected_rewards = rules_rewards
        rules_proposal.expected_objective = rules_objective

        # Update trust values:

        old_trust_rules = self._rules_inferer_trust
        old_trust_adaptive = self._adaptive_inferer_trust
        if (
            self._adaptive_inferer._mode == palaestrai.types.Mode.TEST
            or self._adaptive_inferer._actions_proposed
            > self._adaptive_inferer._start_steps
        ):
            self._adaptive_inferer_trust = Discriminator._pt1(
                old_trust_adaptive, adaptive_objective, self._pt1_period
            )
            self._rules_inferer_trust = Discriminator._pt1(
                old_trust_rules, rules_objective, self._pt1_period
            )

        # Make the choice:

        inferer_choice = InfererChoice.RULES
        applied_setpoints = rules_proposal.actuator_setpoints
        if self._adaptive_inferer_trust > self._rules_inferer_trust:
            self._adaptive_inferer._mode = palaestrai.types.Mode.TEST
            inferer_choice = InfererChoice.ADAPTIVE
            applied_setpoints = adaptive_proposal.actuator_setpoints

        LOG.debug(
            "%s decides to use the %s policy. "
            "Updates trust values: "
            "adaptive(%f => %f), rules(%f => %f); "
            "objectives: %f (adaptive) vs. %f (rules).",
            self,
            (
                "rules"
                if self._rules_inferer_trust >= self._adaptive_inferer_trust
                else "adaptive"
            ),
            old_trust_adaptive,
            self._adaptive_inferer_trust,
            old_trust_rules,
            self._rules_inferer_trust,
            adaptive_objective,
            rules_objective,
        )

        return Decision(
            inferer=inferer_choice,
            setpoints=applied_setpoints,
            rules_inferer_proposal=rules_proposal,
            adaptive_inferer_proposal=adaptive_proposal,
        )

    def _calc_objective(
        self,
        setpoints: List[ActuatorInformation],
        state: List[SensorInformation],
    ):
        rewards = self._reward(state)

        self._memory.append(
            muscle_uid="discriminator",
            sensor_readings=state,
            actuator_setpoints=setpoints,
            rewards=rewards,
        )
        try:
            objective = self._objective.internal_reward(self._memory)
        except Exception as e:  # Whatever may happen in userland...
            LOG.error(
                "Could not calculate objective %s: %s. "
                "Will substitute with 0.0 just to be defensive, "
                "but your results will be screwed.",
                self._objective,
                e,
            )
            objective = 0.0
        return objective, rewards

    def __str__(self) -> str:
        return f"Discriminator(uid={self.uid})"
