import networkx as nx
import os
from nn2eqcdt.src.graph_methods.access_graph_methods import get_root_node
from nn2eqcdt.src.graph_methods.access_graph_methods import get_final_sat_paths_for_region_plotting

class RulesRepo:
    # First idea of a rules repo

    # List of rules present in the rules repo

    def __init__(self):
        # initialisation of a rules repo here
        self.rules = []
        # contains all trees
        self.forest = []
        # read in trees from file
        for filename in os.listdir("../forest"):
            f = os.path.join("../forest", filename)
            # checking if it is a file
            if os.path.isfile(f):
                self.forest.append(nx.read_adjlist(f))

    def import_rules(self):
        # imports new expert knowledge
        pass

    def export_rules(self):
        # exports paths of decision Trees (rules)
        self.rules.clear()
        for tree in self.forest:
            root = get_root_node(tree)
            self.rules.extend(get_final_sat_paths_for_region_plotting(tree, root)[0])
        return self.rules

    def generate_trajectories(self):
        # generate trajectories from rules for rules policy
        pass

    def store_decision_tree(self, tree):
        # store rules in a file
        self.forest.append(tree)
        for tree in self.forest:
            if os.path.isfile("../forest/tree" + str(self.forest.index(tree))):
                os.remove("../forest/tree" + str(self.forest.index(tree)))
            nx.write_adjlist(tree, "../forest/tree" + str(self.forest.index(tree)))

    def return_decision_trees(self):
        return self.forest

    def reset_repo(self):
        # deletes all trees
        for tree in self.forest:
            if os.path.isfile("../forest/tree" + str(self.forest.index(tree))):
                os.remove("../forest/tree" + str(self.forest.index(tree)))
        self.forest.clear()

    def export_tvl(self):
        # exports paths as tenary vector lists; currently works for exactly one tree
        for tree in self.forest:
            tvl = [["-"] * (tree.number_of_nodes()) for i in range(len(self.rules))]
            i = 0
            for rule in self.rules:
                for step in rule:
                    if tree[step[0]][step[1]]["weight"] == 1:
                        tvl[i][step[0]] = 1
                    elif tree[step[0]][step[1]]["weight"] == 0:
                        tvl[i][step[0]] = 0
                i = i + 1

        return tvl
