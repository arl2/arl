from __future__ import annotations
from typing import TYPE_CHECKING, List

from enum import Enum
from dataclasses import dataclass

if TYPE_CHECKING:
    from .decision_proposal import DecisionProposal
    from palaestrai.agent import ActuatorInformation


class InfererChoice(Enum):
    NONE = 0
    RULES = 1
    ADAPTIVE = 2


@dataclass
class Decision:
    inferer: InfererChoice
    setpoints: List[ActuatorInformation]
    rules_inferer_proposal: DecisionProposal
    adaptive_inferer_proposal: DecisionProposal
