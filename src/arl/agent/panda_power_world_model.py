from __future__ import annotations
from typing import TYPE_CHECKING, Optional, List, Callable

import re
from copy import deepcopy

import pandapower as pp

from palaestrai.util.dynaloader import load_with_params
from palaestrai.types import Discrete

from .world_model import WorldModel

if TYPE_CHECKING:
    from palaestrai.agent import SensorInformation, ActuatorInformation


class PandaPowerWorldModel(WorldModel):
    """A world model based on a PandaPower grid"""

    TRAFO_SENSOR_SENSOR_RE = re.compile(r"trafo-(\d+)\.tap_pos")
    LOAD_SGEN_SENSOR_RE = re.compile(r"(load|sgen)-(\d+)-(\d+)\.(p_mw|q_mvar)")

    def __init__(self, grid_builder: str, **kwargs):
        super().__init__()
        self._grid_builder_fn_str = grid_builder
        self._grid_params = kwargs
        self._grid: Optional[pp.pandapowerNet] = None

    @property
    def grid(self) -> pp.pandapowerNet:
        if self._grid is None:
            self._grid = load_with_params(
                self._grid_builder_fn_str, self._grid_params
            )
        return self._grid

    def predict(self):
        pp.runpp(self.grid)

    def evaluate(
        self,
        readings: List[SensorInformation],
        proposals: List[ActuatorInformation],
    ) -> List[SensorInformation]:
        for proposal in proposals:
            self._apply_action_to_grid(proposal)
        self.predict()
        results = deepcopy(readings)
        return list(map(self._update_sensor_from_grid, results))

    def _apply_action_to_grid(self, action: ActuatorInformation):
        if "trafo" in action.uid:
            pattern = PandaPowerWorldModel.TRAFO_SENSOR_SENSOR_RE
            match = re.search(pattern, action.uid)
            item_id = int(match.group())
            self.grid.trafo.at[item_id, "tap_pos"] = action.value
        else:
            pattern = PandaPowerWorldModel.LOAD_SGEN_SENSOR_RE
            match = re.search(pattern, action.uid)
            item, item_id, parameter = match.group(1, 2, 4)
            self.grid[item].at[int(item_id), parameter] = action.value

    def _update_sensor_from_grid(self, sensor: SensorInformation):
        *prefix, params, unit = sensor.uid.split(".")
        _, item, *at = params.split("-")
        if at and not isinstance(sensor.space, Discrete):
            # If `at` is empty, it's the grid health sensor, we don't touch it;
            # and the sensors with discrete spaces are in the input model.
            sensor.value = sensor.space.reshape_to_space(
                self.grid[f"res_{item}"].at[int(at[0]), unit],
            )
        return sensor
