from __future__ import annotations
from typing import TYPE_CHECKING, List, Any

from dataclasses import dataclass

if TYPE_CHECKING:
    from palaestrai.agent import (
        SensorInformation,
        ActuatorInformation,
        RewardInformation,
    )


@dataclass
class DecisionProposal:
    sensor_readings: List[SensorInformation]
    actuator_setpoints: List[ActuatorInformation]
    expected_rewards: List[RewardInformation]
    expected_objective: float
    data_for_brain: Any
