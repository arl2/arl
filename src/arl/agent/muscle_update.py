from __future__ import annotations
from typing import Any

from dataclasses import dataclass


@dataclass
class MuscleUpdate:
    """An update from the ARLBrain to the ARLMuscle

    Parameters
    ----------

    policy_weights : Any
        New weights for the :class:`harl.sac.SACInferer` adaptive policy
    equivalent_representation : Any
        An equivalent representation of the agent's policy that is stored in
        the ::`~RulesRepo`.
    """
    policy_weights: Any
    equivalent_representation: Any