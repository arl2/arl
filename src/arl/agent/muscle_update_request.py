from __future__ import annotations
from typing import TYPE_CHECKING, Any

from dataclasses import dataclass

from .decision import InfererChoice


@dataclass
class MuscleUpdateRequest:
    inferer_chosen: InfererChoice
    adaptive_policy_input: Any
    rules_policy_input: Any
