from __future__ import annotations
from typing import TYPE_CHECKING, List

from abc import ABC, abstractmethod

if TYPE_CHECKING:
    from palaestrai.agent import SensorInformation, ActuatorInformation


class WorldModel(ABC):
    @abstractmethod
    def evaluate(
        self,
        readings: List[SensorInformation],
        proposals: List[ActuatorInformation],
    ) -> List[SensorInformation]:
        """Evaluates potential agent actions ("what if...?")

        This method receives the current state of the environment as perceived
        by the agent,
        as well as the actions the agent would execute,
        and returns a new list of ::`palaestrai.agent.SensorInformaiton`
        objects that the agent would perceive at the next step if it had
        executed the proposed actions.

        Parameters
        ----------
        readings : List of ::`palaestrai.agent.SensorInformation`
            The current state, $s_t$, the agent has perceived.
        proposals : List of ::`palaestrai.agent.ActuatorInformation`
            The actions the agent would like to execute at this time

        Returns
        -------
        readings : List of ::`palaestrai.agent.SensorInformation`
            New readings for the agent; i.e., an world-model-based educated
            guess for $s_{t+1}$
        """
        pass
