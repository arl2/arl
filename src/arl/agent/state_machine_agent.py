from __future__ import annotations

import sys
import random
from random import randint

from transitions import Machine
import logging
from typing import TYPE_CHECKING, Optional, Dict, Any

import numpy as np
import yaml

from palaestrai.agent import Muscle
from palaestrai.types import Box

if TYPE_CHECKING:
    from palaestrai.agent import SensorInformation, ActuatorInformation

LOG = logging.getLogger("palaestrai.agent.Muscle.ReactivePowerMuscle")


class StateMachineAgent(Muscle):
    # This mapping echoes the mapping in the MIDAS config. The MIDAS config
    # is authoritative. This happens to be here because we need to map
    # sensor-actuator (bus voltage - q value), but can't peek at the MIDAS
    # config.
    SENSOR_ACTUATOR_MAPPING = {
        # Sensor ID : Actuator ID
        "0-bus-3.vm_pu": "Photovoltaic-0.q_set_mvar",
        "0-bus-4.vm_pu": "Photovoltaic-1.q_set_mvar",
        "0-bus-5.vm_pu": "Photovoltaic-2.q_set_mvar",
        "0-bus-6.vm_pu": "Photovoltaic-3.q_set_mvar",
        "0-bus-7.vm_pu": "Photovoltaic-4.q_set_mvar",
        "0-bus-8.vm_pu": "Photovoltaic-5.q_set_mvar",
        "0-bus-9.vm_pu": "Photovoltaic-6.q_set_mvar",
        "0-bus-11.vm_pu": "Photovoltaic-7.q_set_mvar",
        "0-bus-13.vm_pu": "Photovoltaic-8.q_set_mvar",
    }

    STEP_SIZE = 5.0

    def __init__(
        self,  # statemachine,
        sensor_actuator_mapping: Optional[Dict] = None,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self._sensor_actuator_mapping = (
            sensor_actuator_mapping
            if sensor_actuator_mapping
            else StateMachineAgent.SENSOR_ACTUATOR_MAPPING
        )
        self.experiment = 1
        self._statemachine = AgentStateMachine(self.experiment)  # statemachine
        self._setpoints = {}

    def setup(self):
        pass

    def propose_actions(
        self, sensors, actuators_available, is_terminal=False
    ) -> tuple:
        LOG.debug(
            "%s: sensors: %s, actuators available: %s",
            self,
            sensors,
            actuators_available,
        )
        self._statemachine.update_soc_battery(sensors[0].value)
        relevant_sensors = {
            short_sid: sensor
            for short_sid in self._sensor_actuator_mapping.keys()
            for sensor in sensors
            if short_sid in sensor.sensor_id
        }
        relevant_actuators = {
            short_aid: actuator
            for short_aid in self._sensor_actuator_mapping.values()
            for actuator in actuators_available
            if short_aid in actuator.actuator_id
        }

        LOG.debug("%s => %s", relevant_sensors, relevant_actuators)

        def _clip_reshape_value(
            setpoint: Any, actuator: ActuatorInformation
        ) -> np.array:
            assert isinstance(actuator.space, Box)
            actuator_box: Box = actuator.space
            _clipped_reshaped_value = actuator_box.reshape_to_space(
                np.clip(
                    setpoint,
                    a_min=actuator_box.low,
                    a_max=actuator_box.high,
                )
            )
            return _clipped_reshaped_value

        def _set_actuator_for_sensor(short_id, sensor: SensorInformation):
            sensor_reading = sensor()
            aid = self._sensor_actuator_mapping[short_id]
            prev_setpoint = self._setpoints.get(aid, None)
            actuator: ActuatorInformation = relevant_actuators[aid]
            setpoint = _clip_reshape_value(
                StateMachineAgent._setpoint(
                    sensor_reading,
                    prev_setpoint,
                    self._statemachine.constraints,
                    self._statemachine.state,
                    self.experiment,
                    actuator,
                ),
                actuator,
            )
            self._setpoints[aid] = setpoint

            actuator(setpoint)
            return actuator

        new_setpoints = [
            _set_actuator_for_sensor(ssid, sensor)
            for ssid, sensor in relevant_sensors.items()
            if self._sensor_actuator_mapping[ssid] in relevant_actuators
        ]
        if self._statemachine.lazy in range(
            self._statemachine.step_intervals[self._statemachine.state][0],
            self._statemachine.step_intervals[self._statemachine.state][1],
        ):
            if randint(0, 5) == 1:
                if self.experiment == 2 and self._statemachine.state == "s1":
                    if randint(0, 1) == 1:
                        self._statemachine.step()
                    else:
                        self._statemachine.step_b()
                else:
                    self._statemachine.step()
            else:
                self._statemachine.keep()
        elif (
            self._statemachine.lazy
            < self._statemachine.step_intervals[self._statemachine.state][0]
        ):
            self._statemachine.keep()
        elif (
            self._statemachine.lazy
            >= self._statemachine.step_intervals[self._statemachine.state][1]
        ):
            if self.experiment == 2 and self._statemachine.state == "s1":
                if randint(0, 1) == 1:
                    self._statemachine.step()
                else:
                    self._statemachine.step_b()
            else:
                self._statemachine.step()

        for a in [a for a in actuators_available if a not in new_setpoints]:
            clipped_reshaped_value = _clip_reshape_value(0.0, a)
            a(clipped_reshaped_value)
            new_setpoints.append(a)

        LOG.debug("%s: new setpoints: %s", self, new_setpoints)
        return new_setpoints

    @staticmethod
    def _setpoint(
        sensor_value, prev_setpoint, constraints, state, experiment, actuator
    ):
        if prev_setpoint is None:
            return 0.0

        if experiment == 1:
            return float(
                random.SystemRandom().uniform(
                    constraints[state][str(actuator.actuator_id)][0],
                    constraints[state][str(actuator.actuator_id)][1],
                )
            )
        return float(
            random.SystemRandom().uniform(
                constraints[state][0], constraints[state][1]
            )
        )

    def update(self, update):
        pass

    def prepare_model(self):
        pass

    def __repr__(self):
        pass

    def __str__(self):
        return f"{self.__class__}(id=0x{id(self):x})"


class AgentStateMachine(object):
    def __init__(self, experiment):
        self.experiment = experiment
        self.soc_battery = 50
        self.step_intervals = {}
        self.step_intervals["s1"] = [20, 200]
        self.step_intervals["s2"] = [20, 200]
        if self.experiment == 2:
            self.step_intervals["s3"] = [20, 200]
        if self.experiment == 1:
            # this is a very explicit implementation; however, reading out this way is the way to go for file adaptation
            self.constraints = {}
            self.constraints["s1"] = {}
            self.constraints["s1"][
                "midas_powergrid.Pysimmods-0.Battery-0.p_set_mw"
            ] = [0.8, 0.8]
            self.constraints["s1"][
                "midas_powergrid.Pysimmods-0.Photovoltaic-5.q_set_mvar"
            ] = [5.6, 6.6]
            self.constraints["s2"] = {}
            self.constraints["s2"][
                "midas_powergrid.Pysimmods-0.Battery-0.p_set_mw"
            ] = [-0.8, -0.8]
            self.constraints["s2"][
                "midas_powergrid.Pysimmods-0.Photovoltaic-5.q_set_mvar"
            ] = [-5.6, -6.6]

        else:
            self.constraints = {}
            self.constraints["s1"] = [0.8, 0.8]
            self.constraints["s2"] = [-0.8, -0.8]
            if self.experiment == 2:
                self.constraints["s3"] = [0.0, 0.0]

        self.initial_state = "s1"
        if self.experiment == 2:
            self.states = ["s1", "s2", "s3"]
        else:
            self.states = ["s1", "s2"]

        self.machine = Machine(
            model=self, states=self.states, initial=self.initial_state
        )
        if self.experiment == 2:
            self.machine.add_transition(
                trigger="step", source="s1", dest="s2", after="print1"
            )
            self.machine.add_transition(
                trigger="step", source="s2", dest="s1", after="print1"
            )
            self.machine.add_transition(
                trigger="step_b", source="s1", dest="s3", after="print1"
            )
            self.machine.add_transition(
                trigger="step", source="s3", dest="s1", after="print1"
            )

            self.machine.add_transition(
                trigger="keep", source="s1", dest="s1", after="print2"
            )
            self.machine.add_transition(
                trigger="keep", source="s2", dest="s2", after="print2"
            )
            self.machine.add_transition(
                trigger="keep", source="s3", dest="s3", after="print2"
            )

        else:
            if self.experiment == 3:
                self.machine.add_transition(
                    trigger="step",
                    source="s1",
                    dest="s2",
                    after="print1",
                    conditions=["battery_half_loaded"],
                )
            else:
                self.machine.add_transition(
                    trigger="step", source="s1", dest="s2", after="print1"
                )

            self.machine.add_transition(
                trigger="step", source="s2", dest="s1", after="print1"
            )
            self.machine.add_transition(
                trigger="keep", source="s1", dest="s1", after="print2"
            )
            self.machine.add_transition(
                trigger="keep", source="s2", dest="s2", after="print2"
            )
        self.lazy = 0

    def print1(self):
        # print(self.state + " stepping forward")
        self.lazy = 0

    def print2(self):
        self.lazy = self.lazy + 1
        # print(self.state + " keep state this is the " + str(self.lazy) + ". time")

    def battery_half_loaded(self):
        return self.soc_battery > 50

    def update_soc_battery(self, soc_battery):
        self.soc_battery = soc_battery
