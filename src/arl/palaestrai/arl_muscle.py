from __future__ import annotations
from typing import TYPE_CHECKING, Optional, Dict, List, Tuple

import logging

from arl.agent.world_model import WorldModel
from midas_palaestrai.reward import Reward
from palaestrai.agent import (
    Muscle,
    SensorInformation,
    ActuatorInformation,
    Objective,
)
from palaestrai.util.dynaloader import load_with_params

from arl.agent import (
    Decision,
    Discriminator,
    InfererChoice,
    MuscleUpdateResponse,
)

if TYPE_CHECKING:
    from ..agent.decision import Decision


LOG = logging.getLogger(__name__)


class ARLMuscle(Muscle):
    DEFAULT_CONFIG_ADAPTIVE_POLICY = dict(name="harl:SACMuscle", params=dict())
    DEFAULT_CONFIG_RULES_POLICY = dict(
        name="midas.tools.palaestrai:ReactivePowerMuscle",
        params=dict(),
    )
    DEFAULT_CONFIG_WORLD_MODEL = dict(
        name="arl.agent.panda_power_world_model:PandaPowerWorldModel",
        params=dict(
            grid_builder="midas.modules.powergrid.custom.midasmv:build_grid"
        ),
    )
    DEFAULT_CONFIG_REWARD = dict(
        name="midas.tools.palaestrai.rewards:ExtendedGridHealthReward",
        params=dict(),
    )
    DEFAULT_CONFIG_OBJECTIVE = dict(
        name="midas.tools.palaestrai:ArlDefenderObjective",
        params=dict(),
    )

    def __init__(
        self,
        adaptive_policy: Optional[Dict] = None,
        rules_policy: Optional[Dict] = None,
        world_model: Optional[Dict] = None,
        reward: Optional[Dict] = None,
        objective: Optional[Dict] = None,
        imitation_steps: Optional[int] = None,
    ):
        super().__init__()
        self._discriminator: Optional[Discriminator] = None

        self._adaptive_policy: Optional[Muscle] = None
        self._adaptive_policy_config = (
            adaptive_policy or ARLMuscle.DEFAULT_CONFIG_ADAPTIVE_POLICY
        )
        self._rules_policy: Optional[Muscle] = None
        self._rules_policy_config = (
            rules_policy or ARLMuscle.DEFAULT_CONFIG_RULES_POLICY
        )
        self._world_model: Optional[WorldModel] = None
        self._world_model_config = (
            world_model or ARLMuscle.DEFAULT_CONFIG_WORLD_MODEL
        )
        self._reward: Optional[Reward] = None
        self._reward_config = reward or ARLMuscle.DEFAULT_CONFIG_REWARD
        self._objective: Optional[Objective] = None
        self._objective_config = (
            objective or ARLMuscle.DEFAULT_CONFIG_OBJECTIVE
        )
        self._imitation_steps: Optional[int] = imitation_steps

    def setup(self):
        LOG.info(
            "%s loads adaptive policy: %s", self, self._adaptive_policy_config
        )
        self._adaptive_policy = load_with_params(
            self._adaptive_policy_config["name"],
            self._adaptive_policy_config["params"],
        )
        self._adaptive_policy.setup()

        LOG.info("%s loads rules policy: %s", self, self._rules_policy_config)
        self._rules_policy = load_with_params(
            self._rules_policy_config["name"],
            self._rules_policy_config["params"],
        )
        self._rules_policy.setup()

        self._world_model = load_with_params(
            self._world_model_config["name"],
            self._world_model_config["params"],
        )
        self._reward: Reward = load_with_params(
            self._reward_config["name"], self._reward_config["params"]
        )
        self._objective: Objective = load_with_params(
            self._objective_config["name"], self._objective_config["params"]
        )

        self._discriminator = Discriminator(
            adaptive_inferer=self._adaptive_policy,
            rules_inferer=self._rules_policy,
            world_model=self._world_model,
            reward=self._reward,
            objective=self._objective,
            imitation_steps=self._imitation_steps,
        )
        self._discriminator.uid = self.uid

    def teardown(self):
        exc = None
        if self._adaptive_policy is not None:
            try:
                self._adaptive_policy.teardown()
            except Exception as e:
                LOG.error(
                    "%s could not teardown its adaptive policy: %s", self, e
                )
                exc = e
        if self._rules_policy is not None:
            try:
                self._rules_policy.teardown()
            except Exception as e:
                LOG.error(
                    "%s could not teardown its rules policy: %s", self, e
                )
                exc = e
        if exc is not None:
            raise exc

    @property
    def discriminator(self):
        assert self._discriminator is not None
        return self._discriminator

    def propose_actions(
        self,
        sensor_readings: List[SensorInformation],
        actuators_available: List[ActuatorInformation],
    ) -> Tuple[List[ActuatorInformation], Decision]:
        # Explicitly set mode:
        self._adaptive_policy._mode = self._mode
        self._rules_policy._mode = self._mode
        LOG.debug(
            "%s querying policies for actions in mode %s (%s, %s)",
            self,
            self._mode,
            self._rules_policy._mode,
            self._rules_policy._mode,
        )

        decision: Decision = self.discriminator.propose_actions(
            sensor_readings, actuators_available
        )
        self.add_statistics(
            "adaptive_inferer_trust",
            self.discriminator.adaptiver_inferer_trust,
        )
        self.add_statistics(
            "rules_inferer_trust", self.discriminator.rules_inferer_trust
        )
        return (
            (
                decision.rules_inferer_proposal.actuator_setpoints
                if decision.inferer == InfererChoice.RULES
                else decision.adaptive_inferer_proposal.actuator_setpoints
            ),
            decision,
        )

    def update(self, update: MuscleUpdateResponse):
        if update.adaptive_policy_update is not None:
            self._adaptive_policy.update(update.adaptive_policy_update)
        if update.rules_policy_update is not None:
            self._rules_policy.update(update.rules_policy_update)

    def __str__(self):
        return super().__str__() + (
            f"\n"
            f"rules_policy={str(self._rules_policy)}, "
            f"adaptive_policy={str(self._adaptive_policy)}"
        )
