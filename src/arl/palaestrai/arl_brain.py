from __future__ import annotations
from typing import Any, Optional, Dict

import numpy as np
from dataclasses import field

from palaestrai.agent import (
    Brain,
)
from palaestrai.types import Mode
from palaestrai.util.dynaloader import load_with_params
from arl.agent import (
    Decision,
    MuscleUpdateResponse,
    MuscleUpdateRequest,
    InfererChoice,
)


class ARLBrain(Brain):
    def __init__(
        self,
        adaptive_brain: Dict = field(
            default_factory=lambda: dict(name="harl:SACBrain", params=dict())
        ),
    ):
        super().__init__()

        self._adaptive_brain: Optional[Brain] = None
        self._adaptive_brain_config = adaptive_brain
        self.memory.size_limit = 2
        self._brains_initialized = False

    def setup(self):
        ab_config = self._adaptive_brain_config
        self._adaptive_brain: Brain = load_with_params(
            ab_config["name"], ab_config["params"]
        )
        assert self._adaptive_brain is not None
        self._adaptive_brain._seed = self._seed
        self._adaptive_brain._memory = self._memory
        self._adaptive_brain._sensors = self._sensors
        self._adaptive_brain._actuators = self._actuators
        self._adaptive_brain._dumpers = self._dumpers

        self._adaptive_brain.try_load_brain_dump()

        self._adaptive_brain.setup()

    def thinking(self, muscle_id: str, data_from_muscle: Decision) -> Any:
        self._adaptive_brain.mode = self.mode
        if self.mode != Mode.TRAIN and self._brains_initialized:
            return MuscleUpdateResponse(
                rules_policy_update=None,
                adaptive_policy_update=None,
            )
        else:  # Skip rest in the future if we're not training
            self._brains_initialized = True
        data_for_brain = None
        if (
            data_from_muscle is not None
            and data_from_muscle.inferer == InfererChoice.RULES
            and data_from_muscle.rules_inferer_proposal.data_for_brain
            is not None
        ):
            data_for_brain = (
                data_from_muscle.rules_inferer_proposal.data_for_brain
            )
        if (
            data_from_muscle is not None
            and data_from_muscle.inferer == InfererChoice.ADAPTIVE
            and data_from_muscle.adaptive_inferer_proposal.data_for_brain
            is not None
        ):
            data_for_brain = (
                data_from_muscle.adaptive_inferer_proposal.data_for_brain
            )

        update1 = self._adaptive_brain.thinking(muscle_id, data_for_brain)
        # calc_dt(self._adaptive_brain)

        # Make sure we initialize the SACMuscle correctly before the first
        # actual step:

        if data_from_muscle is None and update1 is not None:
            return MuscleUpdateResponse(
                adaptive_policy_update=update1, rules_policy_update=None
            )

        # Now, let's also learn from a proposal that was gauged by the
        # Discriminator, even if it seems to be not as good:

        data_for_brain = None
        if (
            data_from_muscle is not None
            and data_from_muscle.inferer != InfererChoice.RULES
            and data_from_muscle.rules_inferer_proposal.data_for_brain
            is not None
        ):
            data_for_brain = (
                data_from_muscle.rules_inferer_proposal.data_for_brain
            )
            self._adaptive_brain.memory.append(
                muscle_uid=muscle_id,
                sensor_readings=data_from_muscle.rules_inferer_proposal.sensor_readings,
                actuator_setpoints=data_from_muscle.rules_inferer_proposal.actuator_setpoints,
                rewards=data_from_muscle.rules_inferer_proposal.expected_rewards,
                objective=np.array(
                    [
                        data_from_muscle.rules_inferer_proposal.expected_objective
                    ]
                ),
                done=self.memory.tail(1).dones.item(),
            )
        if (
            data_from_muscle is not None
            and data_from_muscle.inferer != InfererChoice.ADAPTIVE
            and data_from_muscle.adaptive_inferer_proposal.data_for_brain
            is not None
        ):
            data_for_brain = (
                data_from_muscle.adaptive_inferer_proposal.data_for_brain
            )
            self._adaptive_brain.memory.append(
                muscle_uid=muscle_id,
                sensor_readings=data_from_muscle.adaptive_inferer_proposal.sensor_readings,
                actuator_setpoints=data_from_muscle.adaptive_inferer_proposal.actuator_setpoints,
                rewards=data_from_muscle.adaptive_inferer_proposal.expected_rewards,
                objective=np.array(
                    [
                        data_from_muscle.adaptive_inferer_proposal.expected_objective
                    ]
                ),
                done=self.memory.tail(1).dones.item(),
            )
        update2 = self._adaptive_brain.thinking(muscle_id, data_for_brain)
        # calc_dt(self._adaptive_brain)

        return MuscleUpdateResponse(
            adaptive_policy_update=(
                update2 if update2 is not None else update1
            ),
            rules_policy_update=None,
        )

    def store(self):
        if self._adaptive_brain is None:
            return
        self._adaptive_brain.store()

    def load(self):
        if self._adaptive_brain is None:
            return
        self._adaptive_brain.load()

    def __str__(self):
        return super().__str__() + (
            f"\n" f"adaptive_brain={str(self._adaptive_brain)}"
        )
