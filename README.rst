Adversarial Resilience Learning
===============================

.. image:: https://gitlab.com/uploads/-/system/project/avatar/40036553/ARL-Logo.png
  :width: 200
  :alt: Alternative text

Adversarial Resilience Learning (ARL) describes both a methodology as well as
its reference implementation. In the ARL methodology, two (or more) competing
agents (“attacker” and “defender”) compete for control of a Critical
National Infrastructure (CNI). Through this competition, the two agents
learn robust strategies for a resilient control of the CNI. The research
group develops an advanced architecture on top of it, featuring higher-order
Deep Reinforcement Learning algorithms, extraction of learned strategies in
a non-standard logic via methods of Explainable Reinforcement Learning, and
learning from domain-expert knowledge.

This repository houses documentation (code and scientific, where applicable)
as well as the ARL reference implementation.