import unittest
import numpy as np
from palaestrai.agent import SensorInformation, ActuatorInformation
from palaestrai.types import Discrete, Box
from src.arl.agent.panda_power_world_model import PandaPowerWorldModel


class TestPandaPowerWorldModel(unittest.TestCase):
    def test(self):
        model = PandaPowerWorldModel(
            grid_builder="midas.modules.powergrid.custom.midasmv:build_grid"
        )

        actuators = [
            ActuatorInformation(
                action_space=Box(low=0, high=1.4),
                setpoint=[1.0],
                actuator_id="midas_powergrid.Powergrid-0.0-load-5-7.p_mw",
            ),
            ActuatorInformation(
                action_space=Box(low=-0.26, high=0),
                setpoint=[0],
                actuator_id="midas_powergrid.Powergrid-0.0-sgen-9-11.q_mvar",
            ),
            ActuatorInformation(
                action_space=Box(low=-10, high=10),
                setpoint=[0],
                actuator_id="midas_powergrid.Powergrid-0.0-trafo-0.tap_pos",
            ),
        ]

        sensors = [
            SensorInformation(
                observation_space=Box(low=0.0, high=1.2),
                sensor_id="midas_powergrid.Powergrid-0.0-bus-14.vm_pu",
            ),
            SensorInformation(
                observation_space=Box(low=-180.0, high=180.0),
                sensor_id="midas_powergrid.Powergrid-0.0-bus-7.va_degree",
            ),
            SensorInformation(
                observation_space=Discrete(2),
                sensor_id="midas_powergrid.Powergrid-0.0-bus-1.in_service",
            ),
            SensorInformation(
                observation_space=Box(low=0.0, high=200.0),
                sensor_id="midas_powergrid.Powergrid-0.0-line-14.loading_percent",
            ),
            SensorInformation(
                observation_space=Box(low=0, high=np.inf),
                sensor_id="midas_powergrid.Powergrid-0.0-load-6-8.p_mw",
            ),
            SensorInformation(
                observation_space=Box(low=0, high=np.inf),
                sensor_id="midas_powergrid.Powergrid-0.0-sgen-10-12.q_mvar",
            ),
            SensorInformation(
                observation_space=Discrete(2),
                sensor_id="midas_powergrid.Powergrid-0.0-switch-0.closed",
            ),
            SensorInformation(
                observation_space=Box(low=0.0, high=200.0),
                sensor_id="midas_powergrid.Powergrid-0.0-trafo-1.loading_percent",
            ),
            SensorInformation(
                observation_space=Box(low=0, high=1.2),
                sensor_id="midas_powergrid.Powergrid-0.Grid-0.health",
            ),
        ]

        result = model.evaluate(sensors, actuators)


if __name__ == "__main__":
    unittest.main()
