*** Settings ***
Documentation   Integration Test of ARL Scenario

Library         String
Library         Process
Library         OperatingSystem

*** Keywords ***
Clean Files
    Remove File                     ${TEMPDIR}${/}stdout.txt
    Remove File                     ${TEMPDIR}${/}stderr.txt

*** Test Cases ***
Run PalaestrAI Experiment
    ${EXPERIMENT_RUN_FILES_PATH}        List Files In Directory    ${CURDIR}${/}..${/}fixtures    ARL-Experiment_run-*.yml    absolute
    FOR    ${experiment_file}    IN    @{EXPERIMENT_RUN_FILES_PATH}
        Log Many                        ${experiment_file}
        ${result} =                     Run Process     palaestrai    -c    ${CURDIR}${/}..${/}..${/}palaestrai.conf    -vv       experiment-start       ${experiment_file}      cwd=${CURDIR}${/}..${/}fixtures  stdout=${TEMPDIR}${/}stdout.txt  stderr=${TEMPDIR}${/}stderr.txt
        Log Many                        ${result.stdout}    ${result.stderr}
        Should Be Equal As Integers     ${result.rc}  0
    END

