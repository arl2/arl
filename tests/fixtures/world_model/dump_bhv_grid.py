#!/usr/bin/env python3

from pathlib import Path
import pandapower as pp
import midas.modules.powergrid.custom.bhv


def dump_bhv_grid(to: Path):
    grid = midas.modules.powergrid.custom.bhv.build_grid()
    pp.to_json(grid, str(to / "bhv_grid.json"))


if __name__ == "__main__":
    curdir = Path(__file__).parent
    dump_bhv_grid(curdir)
